This is a repository to store all the notebooks I teach in an online classroom.
You are free to copy all or any of these.

To start right away go to [Start Here](./start_here.ipynb).
